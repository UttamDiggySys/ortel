package com.diggysys.ortel.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.diggysys.ortel.R;

public class LoginActivity extends AppCompatActivity {
    CardView login_lay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login_lay = (CardView) findViewById(R.id.login_button);

        login_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(LoginActivity.this,BaseActivity.class);
               startActivity(intent);
               finish();
            }
        });
    }
}
