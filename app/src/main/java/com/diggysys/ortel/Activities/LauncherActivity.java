package com.diggysys.ortel.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.diggysys.ortel.R;

public class LauncherActivity extends AppCompatActivity {
    Animation animMove;
    TextView welcome_txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        animMove = AnimationUtils.loadAnimation(LauncherActivity.this,
                R.anim.move);
        welcome_txt = (TextView)findViewById(R.id.welcome_txt);
        welcome_txt.startAnimation(animMove);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i=new Intent(LauncherActivity.this,LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 5000);
    }
}
